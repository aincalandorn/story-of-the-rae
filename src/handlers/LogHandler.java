package handlers;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

public class LogHandler {
	private Logger log;
	
	public LogHandler(boolean debug){
		log = Logger.getLogger("log");
		log.setUseParentHandlers(debug);
		if(debug){
			log.config("### DEBUGGING ENABLED ###");
		}
		//yyyy/MM/dd HH:mm:ss
		DateFormat df = new SimpleDateFormat("HH-mm-ss-dd-MM-yyyy");
		Date d = new Date();
		String date = df.format(d);
		String file = "errors/error-"+date+".xml";
		
		File f = new File(file);
		if(!f.exists()){
			try{
				f.createNewFile();
			}catch(Exception e){
				
			}
		}
		f=null;
		try {
			log.addHandler(new FileHandler(file));
		}catch(Exception e){
			logError(e);
		}
		
		logMessage("Error log created.");
	}
	public void logError(Exception e){
		log.log(Level.SEVERE, "Error", e);
	}
	public void logMessage(String msg){
		log.info(msg);
	}
	public void logError(String error) {
		log.severe(error);
	}
	public void close(){
		log.info("Game exited by request by the user.");
		Handler[] handlers = log.getHandlers();
		for(Handler h : handlers){
			log.removeHandler(h);
		}
	}
	public void logWarning(String string) {
		log.warning(string);
	}
	
	@Override
	public String toString(){
		return "Logger is active!";
	}
}

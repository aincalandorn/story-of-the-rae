package handlers;

import java.util.ArrayList;

public class FormulaCalc {
	public static int damageTaken(int type, int damage, int intel, int agi, int end){
		if(type==1){
			//melee attack
			damage/= endFactor(end);
		}
		else if(type==2){
			//magic
			damage/= intelFactor(intel);
		}
		double chance = Math.random()+(agiFactor(agi)/5);
		if(chance>0.8){
			damage=-1;
		}
		return damage;
	}
	public static int calcDamage(ArrayList<String> factors,int str, int dex, int intel, int agi, int end, int base){
		int damage = base;
		for(String s:factors){
			switch(s.toLowerCase()){
			case "str":
				damage+=strFactor(str);
				break;
			case "dex":
				damage+=dexFactor(dex);
				break;
			case "intel":
				damage+=intelFactor(intel);
				break;
			case "agi":
				damage+=agiFactor(agi);
				break;
			case "end":
				damage+=endFactor(end);
				break;
			default:
				//nothing!
				break;
			}
		}
		return damage;
	}

	private static int endFactor(int end) {
		return end/5;
	}

	private static int agiFactor(int agi) {
		return agi/3;
	}

	private static int intelFactor(int intel) {
		return intel/2;
	}

	private static int dexFactor(int dex) {
		return dex/3;
	}

	private static int strFactor(int str) {
		return str/2;
	}
}

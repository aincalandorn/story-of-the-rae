package handlers;

import java.util.ArrayList;

import containers.msgs.Parsable;

public class ParseArray {
	private ArrayList<Parsable> list;
	public ParseArray(){
		list=new ArrayList<>();
	}
	public void addParse(String toReplace, String replace){
		list.add(new Parsable(toReplace,replace));
	}
	public ArrayList<Parsable> getArray() {
		return list;
	}

}

package gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import containers.msgs.BtnSetting;
import containers.scenes.Scene;
import handlers.LogHandler;

public class SceneManager implements Runnable {
	private LogHandler logger;
	private Connection conn;

	public SceneManager(LogHandler log) {
		logger = log;
		connect();
	}

	private void connect() {
		conn = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:data/save.db");

		} catch (Exception e) {
			logger.logError(e);
		}
		logger.logMessage("Connection established from SceneManager!");
	}

	public void close() {
		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				logger.logError(e);
			}
		}
		conn = null;
	}

	public void reconnect() {
		connect();
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	public Scene fetchScene(String string) {
		Scene s = new Scene();
		try{
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM scenes WHERE scene=?");
			ps.setString(1,string);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				s.setId(rs.getInt("scene_id"));
				s.setText(rs.getString("sceneText"));
				ps = conn.prepareStatement("SELECT * FROM scene_buttons WHERE scene_id=?");
				ps.setInt(1,s.getId());
				ResultSet rs2 = ps.executeQuery();
				ArrayList<BtnSetting> settings = new ArrayList<>();
				while(rs2.next()){
					settings.add(new BtnSetting(rs2.getInt("button_id"), rs2.getBoolean("enabled"), rs2.getString("text"), rs2.getString("action")));
				}
				s.addSettings(settings);
			}
		}catch(Exception e){
			logger.logError(e);
		}
		return s;
	}

}

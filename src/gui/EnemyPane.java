package gui;

import handlers.LogHandler;

import javax.swing.JPanel;

public class EnemyPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2326400958560026597L;
	private LogHandler logger;
	public EnemyPane(LogHandler log){
		logger=log;
		setLayout(null);	
		logger.logMessage("enemypane started");
	}
}

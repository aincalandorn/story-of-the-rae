package gui;

import javax.swing.JPanel;

import containers.entities.*;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JProgressBar;
import java.awt.Color;

public class EntityDisplay extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6524745357394631366L;
	
	private int currHP=5;
	private int maxHP=20;
	private String name="Name";
	private int currMP=5;
	private int maxMP=20;
	private int level=1;
	private int currEXP=1;
	private int maxEXP=5;
	private String status="";
	
	public void setStats(NPC c){
		currHP = c.getHp();
		maxHP = c.getMaxHp();
		currMP = c.getMp();
		maxMP = c.getMaxMp();
		currEXP = c.getExp();
		maxEXP = c.getToLevel();
		name = c.getName();
		level = c.getLevel();
		
	}
	/**
	 * @wbp.parser.constructor
	 */
	public EntityDisplay(NPC c){
		setStats(c);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		this.setLayout(gridBagLayout);
		
		JLabel lblName = new JLabel(name);
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		this.add(lblName, gbc_lblName);
		
		JLabel lblAvatar = new JLabel("Avatar");
		GridBagConstraints gbc_lblAvatar = new GridBagConstraints();
		gbc_lblAvatar.insets = new Insets(0, 0, 5, 5);
		gbc_lblAvatar.gridx = 0;
		gbc_lblAvatar.gridy = 1;
		this.add(lblAvatar, gbc_lblAvatar);
		
		JLabel lblHp = new JLabel("HP");
		GridBagConstraints gbc_lblHp = new GridBagConstraints();
		gbc_lblHp.insets = new Insets(0, 0, 5, 5);
		gbc_lblHp.gridx = 1;
		gbc_lblHp.gridy = 1;
		this.add(lblHp, gbc_lblHp);
		
		JProgressBar hpBar = new JProgressBar();
		hpBar.setForeground(Color.RED);
		GridBagConstraints gbc_hpBar = new GridBagConstraints();
		gbc_hpBar.insets = new Insets(0, 0, 5, 0);
		gbc_hpBar.gridx = 2;
		gbc_hpBar.gridy = 1;
		this.add(hpBar, gbc_hpBar);
		
		JLabel lblMp = new JLabel("MP");
		hpBar.setValue(currHP);
		hpBar.setMaximum(maxHP);
		hpBar.setString(currHP+"/"+maxHP);
		hpBar.setStringPainted(true);
		GridBagConstraints gbc_lblMp = new GridBagConstraints();
		gbc_lblMp.insets = new Insets(0, 0, 5, 5);
		gbc_lblMp.gridx = 1;
		gbc_lblMp.gridy = 2;
		this.add(lblMp, gbc_lblMp);
		
		JLabel lblStatus = new JLabel(status);
		GridBagConstraints gbc_lblStatus = new GridBagConstraints();
		gbc_lblStatus.insets = new Insets(0, 0, 5, 5);
		gbc_lblStatus.gridx = 1;
		gbc_lblStatus.gridy = 0;
		add(lblStatus, gbc_lblStatus);
		
		JProgressBar mpBar = new JProgressBar();
		mpBar.setValue(currMP);
		mpBar.setMaximum(maxMP);
		mpBar.setString(currMP+"/"+maxMP);
		mpBar.setStringPainted(true);
		GridBagConstraints gbc_mpBar = new GridBagConstraints();
		gbc_mpBar.insets = new Insets(0, 0, 5, 0);
		gbc_mpBar.gridx = 2;
		gbc_mpBar.gridy = 2;
		this.add(mpBar, gbc_mpBar);
		
		JLabel lblLevel = new JLabel("Level "+level);
		GridBagConstraints gbc_lblLevel = new GridBagConstraints();
		gbc_lblLevel.insets = new Insets(0, 0, 0, 5);
		gbc_lblLevel.gridx = 0;
		gbc_lblLevel.gridy = 3;
		this.add(lblLevel, gbc_lblLevel);
		
		JLabel lblExp = new JLabel("EXP");
		GridBagConstraints gbc_lblExp = new GridBagConstraints();
		gbc_lblExp.insets = new Insets(0, 0, 0, 5);
		gbc_lblExp.gridx = 1;
		gbc_lblExp.gridy = 3;
		this.add(lblExp, gbc_lblExp);
		
		JProgressBar expBar = new JProgressBar();
		expBar.setForeground(Color.MAGENTA);
		expBar.setValue(currEXP);
		expBar.setMaximum(maxEXP);
		expBar.setString(currEXP+"/"+maxEXP);
		expBar.setStringPainted(true);
		GridBagConstraints gbc_expBar = new GridBagConstraints();
		gbc_expBar.gridx = 2;
		gbc_expBar.gridy = 3;
		this.add(expBar, gbc_expBar);
	}

	public EntityDisplay(Monster m){
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		this.setLayout(gridBagLayout);
		
		JLabel lblName = new JLabel(name);
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		this.add(lblName, gbc_lblName);
		
		JLabel lblAvatar = new JLabel("Avatar");
		GridBagConstraints gbc_lblAvatar = new GridBagConstraints();
		gbc_lblAvatar.insets = new Insets(0, 0, 5, 5);
		gbc_lblAvatar.gridx = 0;
		gbc_lblAvatar.gridy = 1;
		this.add(lblAvatar, gbc_lblAvatar);
		
		JLabel lblHp = new JLabel("HP");
		GridBagConstraints gbc_lblHp = new GridBagConstraints();
		gbc_lblHp.insets = new Insets(0, 0, 5, 5);
		gbc_lblHp.gridx = 1;
		gbc_lblHp.gridy = 1;
		this.add(lblHp, gbc_lblHp);
		
		JProgressBar hpBar = new JProgressBar();
		hpBar.setForeground(Color.RED);
		GridBagConstraints gbc_hpBar = new GridBagConstraints();
		gbc_hpBar.insets = new Insets(0, 0, 5, 0);
		gbc_hpBar.gridx = 2;
		gbc_hpBar.gridy = 1;
		this.add(hpBar, gbc_hpBar);
		
		JLabel lblMp = new JLabel("MP");
		hpBar.setValue(currHP);
		hpBar.setMaximum(maxHP);
		hpBar.setString(currHP+"/"+maxHP);
		hpBar.setStringPainted(true);
		GridBagConstraints gbc_lblMp = new GridBagConstraints();
		gbc_lblMp.insets = new Insets(0, 0, 5, 5);
		gbc_lblMp.gridx = 1;
		gbc_lblMp.gridy = 2;
		this.add(lblMp, gbc_lblMp);
		
		JLabel lblStatus = new JLabel("Status");
		GridBagConstraints gbc_lblStatus = new GridBagConstraints();
		gbc_lblStatus.insets = new Insets(0, 0, 5, 5);
		gbc_lblStatus.gridx = 0;
		gbc_lblStatus.gridy = 2;
		add(lblStatus, gbc_lblStatus);
		
		JProgressBar mpBar = new JProgressBar();
		mpBar.setValue(currMP);
		mpBar.setMaximum(maxMP);
		mpBar.setString(currMP+"/"+maxMP);
		mpBar.setStringPainted(true);
		GridBagConstraints gbc_mpBar = new GridBagConstraints();
		gbc_mpBar.insets = new Insets(0, 0, 5, 0);
		gbc_mpBar.gridx = 2;
		gbc_mpBar.gridy = 2;
		this.add(mpBar, gbc_mpBar);
		
		JLabel lblLevel = new JLabel("Level "+level);
		GridBagConstraints gbc_lblLevel = new GridBagConstraints();
		gbc_lblLevel.insets = new Insets(0, 0, 0, 5);
		gbc_lblLevel.gridx = 0;
		gbc_lblLevel.gridy = 3;
		this.add(lblLevel, gbc_lblLevel);
	}
}

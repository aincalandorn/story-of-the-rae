package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import handlers.LogHandler;

import javax.swing.JLabel;
import javax.swing.JPanel;

import containers.entities.NPC;
import containers.roles.Skill;

public class PartyPane extends JPanel implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5233604687758749663L;
	private LogHandler logger;
	private Connection conn;
	private ArrayList<NPC> chars;

	public PartyPane(LogHandler log) {
		logger = log;
		chars = new ArrayList<>();
		connect();
		getParty();
		rebuildPane();
	}

	private void connect() {
		conn = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:data/save.db");

		} catch (Exception e) {
			logger.logError(e);
		}
		logger.logMessage("Connection established from PartyPane!");
	}

	public void close() {
		if (conn != null) {
			try {
				conn.close();
			} catch (Exception e) {
				logger.logError(e);
			}
		}
		conn = null;
	}

	public void reconnect() {
		connect();
		rebuildPane();
	}

	private void getParty() {
		chars = new ArrayList<>();
		try {
			PreparedStatement sql = conn.prepareStatement("SELECT * FROM entity JOIN party USING (entity_id);");
			ResultSet rs = sql.executeQuery();
			while (rs.next()) {
				int entityID = rs.getInt("entity_id");
				String name = rs.getString("name");
				int hp = rs.getInt("hp");
				int mp = rs.getInt("mp");
				int exp = rs.getInt("exp");
				int toLevel = rs.getInt("toLevel");
				int level = rs.getInt("level");
				int classID = rs.getInt("class_id");
				int topArmour = rs.getInt("topArmour");
				int pants = rs.getInt("pants");
				int boots = rs.getInt("boots");
				int hat = rs.getInt("hat");
				int weapon = rs.getInt("weapon");
				int status = rs.getInt("status");
				int gender = rs.getInt("gender");
				int dex = rs.getInt("dex");
				int str = rs.getInt("str");
				int intel = rs.getInt("intel");
				int end = rs.getInt("end");
				int agi = rs.getInt("agi");
				int currHP = rs.getInt("currHP");
				int currMP = rs.getInt("currMP");
				ArrayList<Skill> skills = new ArrayList<>();
				NPC c = new NPC(entityID, name, hp, mp, exp, toLevel, level,
						classID, topArmour, pants, boots, hat, weapon, status,
						gender, dex, str, intel, end, agi, skills, currHP,
						currMP);
				chars.add(c);
			}
		} catch (Exception e) {
			logger.logError(e);
		}

	}

	public void rebuildPane() {
		getParty();
		this.removeAll();
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0, 0, 5, 5);
		gbc.gridx = 0;

		if (chars.size() > 0) {

			for (int x = 0; x < chars.size(); x++) {
				gbc.gridy = x;
				this.add(new EntityDisplay(chars.get(x)), gbc);
			}
		} else {
			gbc.gridy = 0;
			JLabel temp = new JLabel("<html><body><h1>Party</h1></body></html>");
			temp.setBounds(0, 0, 200, 200);
			this.add(temp, gbc);
		}
		repaint();
		revalidate();
	}

	public void addMember(int id) {
		try {
			PreparedStatement s = conn
					.prepareStatement("INSERT INTO party VALUES(?,?)");
			s.setInt(1, id);
			s.setInt(2,id);
			s.executeUpdate();
		} catch (Exception e) {
			logger.logError(e);
		}
		rebuildPane();
	}

	public void addMember(NPC npc) {
		try {
			PreparedStatement s = conn
					.prepareStatement("INSERT INTO party VALUES(?,?)");
			s.setInt(1, npc.getEntityID());
			s.setInt(2, npc.getEntityID());
			s.executeUpdate();
		} catch (Exception e) {
			logger.logError(e);
		}
		rebuildPane();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}

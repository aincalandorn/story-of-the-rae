package gui;

import handlers.LogHandler;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import java.awt.BorderLayout;

import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;

import java.awt.Dimension;

import javax.swing.JEditorPane;

import launcher.GameInfo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.awt.GridBagLayout;

import javax.swing.JButton;

import containers.msgs.BtnSetting;
import containers.scenes.Scene;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GUI extends JFrame {
	private static final long serialVersionUID = 985279543482921069L;

	private JScrollPane partyTab;
	private JScrollPane questTab;
	private JScrollPane inventoryTab;
	private JEditorPane textBox;
	private GameInfo gi;
	private Controller gui;
	protected boolean debug = false;
	protected PartyPane party;
	protected QuestPane quests;
	protected InvPane inventory;
	protected EnemyPane enemyPanel;
	protected JFrame frame;
	protected JButton btn1;
	protected JButton btn2;
	protected JButton btn3;
	protected JButton btn4;
	protected JButton btn5;
	protected JButton btn6;
	protected JButton btn7;
	protected JButton btn8;
	protected JButton btn9;
	protected JButton btn10;
	protected JTabbedPane tabbedPane;
	protected LogHandler logger;
	protected SceneManager sm;
	
	private void createGUI() {
		setTitle(gi.getTitle());
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setSize(1024, 720);
		setResizable(false);
		frame = this;

		JPanel textPanel = new JPanel();
		getContentPane().add(textPanel, BorderLayout.CENTER);
		textPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		textPanel.add(scrollPane, BorderLayout.CENTER);

		textBox = new JEditorPane();
		textBox.setRequestFocusEnabled(false);
		textBox.setEditable(false);
		textBox.setContentType("text/html");
		scrollPane.setViewportView(textBox);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				confirmExit(frame);
			}

		});

		JPanel controls = new JPanel();
		getContentPane().add(controls, BorderLayout.SOUTH);
		GridBagLayout gbl_controls = new GridBagLayout();
		gbl_controls.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_controls.rowHeights = new int[] { 0, 0, 0 };
		gbl_controls.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		gbl_controls.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		controls.setLayout(gbl_controls);

		btn1 = new JButton("");
		GridBagConstraints gbc_btn1 = new GridBagConstraints();
		gbc_btn1.insets = new Insets(0, 0, 5, 5);
		gbc_btn1.gridx = 0;
		gbc_btn1.gridy = 0;
		btn1.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == '1') {
					btn1.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		controls.add(btn1, gbc_btn1);

		btn2 = new JButton("");
		GridBagConstraints gbc_btn2 = new GridBagConstraints();
		gbc_btn2.insets = new Insets(0, 0, 5, 5);
		gbc_btn2.gridx = 1;
		gbc_btn2.gridy = 0;
		btn2.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == '2') {
					btn2.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		controls.add(btn2, gbc_btn2);

		btn3 = new JButton("");
		GridBagConstraints gbc_btn3 = new GridBagConstraints();
		gbc_btn3.insets = new Insets(0, 0, 5, 5);
		gbc_btn3.gridx = 2;
		gbc_btn3.gridy = 0;
		btn3.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == '3') {
					btn3.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		controls.add(btn3, gbc_btn3);

		btn4 = new JButton("");
		GridBagConstraints gbc_btn4 = new GridBagConstraints();
		gbc_btn4.insets = new Insets(0, 0, 5, 5);
		gbc_btn4.gridx = 3;
		gbc_btn4.gridy = 0;
		btn4.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == '4') {
					btn4.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		controls.add(btn4, gbc_btn4);

		btn5 = new JButton("");
		GridBagConstraints gbc_btn5 = new GridBagConstraints();
		gbc_btn5.insets = new Insets(0, 0, 5, 5);
		gbc_btn5.gridx = 4;
		gbc_btn5.gridy = 0;
		btn5.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == '5') {
					btn5.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		controls.add(btn5, gbc_btn5);

		btn6 = new JButton("");
		GridBagConstraints gbc_btn6 = new GridBagConstraints();
		gbc_btn6.insets = new Insets(0, 0, 0, 5);
		gbc_btn6.gridx = 0;
		gbc_btn6.gridy = 1;
		btn6.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == 'q') {
					btn6.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		controls.add(btn6, gbc_btn6);

		btn7 = new JButton("");
		GridBagConstraints gbc_btn7 = new GridBagConstraints();
		gbc_btn7.insets = new Insets(0, 0, 0, 5);
		gbc_btn7.gridx = 1;
		gbc_btn7.gridy = 1;
		btn7.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == 'w') {
					btn7.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		controls.add(btn7, gbc_btn7);

		btn8 = new JButton("");
		GridBagConstraints gbc_btn8 = new GridBagConstraints();
		gbc_btn8.insets = new Insets(0, 0, 0, 5);
		gbc_btn8.gridx = 2;
		gbc_btn8.gridy = 1;
		btn8.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == 'e') {
					btn8.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		controls.add(btn8, gbc_btn8);

		btn9 = new JButton("");
		GridBagConstraints gbc_btn9 = new GridBagConstraints();
		gbc_btn9.insets = new Insets(0, 0, 0, 5);
		gbc_btn9.gridx = 3;
		gbc_btn9.gridy = 1;
		btn9.addKeyListener(new KeyListener() { // TODO

			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == 'r') {
					btn9.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		controls.add(btn9, gbc_btn9);

		btn10 = new JButton("");
		GridBagConstraints gbc_btn10 = new GridBagConstraints();
		gbc_btn10.insets = new Insets(0, 0, 0, 5);
		gbc_btn10.gridx = 4;
		gbc_btn10.gridy = 1;
		btn10.addKeyListener(new KeyListener() { // TODO
			@Override
			public void keyPressed(KeyEvent k) {
				if (k.getKeyChar() == 't') {
					btn10.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		controls.add(btn10, gbc_btn10);
		
		setDefaultButtons();
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setPreferredSize(new Dimension(300, getContentPane()
				.getHeight()));
		getContentPane().add(tabbedPane, BorderLayout.EAST);

		partyTab = new JScrollPane(party);
		tabbedPane
				.addTab("<html><body><div style=\"width:50px\">Party</div></body></html>",
						null, partyTab, "View current party");

		questTab = new JScrollPane(quests);
		tabbedPane
				.addTab("<html><body><div style=\"width:50px\">Quests</div></body></html>",
						null, questTab, "View journal");

		inventoryTab = new JScrollPane(inventory);
		tabbedPane
				.addTab("<html><body><div style=\"width:50px\">Inventory</div></body></html>",
						null, inventoryTab, "View your inventory");

		tabbedPane.setSelectedIndex(0);

		enemyPanel = new EnemyPane(logger);
		getContentPane().add(enemyPanel, BorderLayout.WEST);

		frame.setVisible(true);

	}

	public GUI(boolean f_debug, GameInfo gi, LogHandler logger) {
		this.logger = logger;
		this.gi = gi;
		gui = new Controller(logger);
		Thread guit = new Thread(gui);
		guit.start();
		createGUI();
		// debug controls
		debug = f_debug;
		String text = "<h1 style=\"text-align:center;\">Welcome!</h1>"
				+ gi.getCredits();
		if (debug) {
			text += gi.getInfo();
		}

		gui.push(text);
	}

	protected void confirmExit(JFrame frame) {
		if (JOptionPane.showConfirmDialog(frame,
				"Are you sure to close this window?", "Really Closing?",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
			gui.save();
			gui.close();
			System.exit(0);
		}
	}
	private void set(int num,boolean enabled,String text,ActionListener list){
		JButton btn;
		switch (num) {
		case 1:  btn = btn1;  break;
		case 2:	 btn = btn2;  break;
		case 3:	 btn = btn3;  break;
		case 4:	 btn = btn4;  break;
		case 5:	 btn = btn5;  break;
		case 6:	 btn = btn6;  break;
		case 7:	 btn = btn7;  break;
		case 8:	 btn = btn8;  break;
		case 9:	 btn = btn9;  break;
		case 10: btn = btn10; break;
		default: 
			btn=null;
		}
		if (btn != null) {
			btn.setEnabled(enabled);
			if (text == null) {
				text = "";
			}
			btn.setText(text);
			for (ActionListener old : btn.getActionListeners()) {
				btn.removeActionListener(old);
			}
			if (list != null) {
				btn.addActionListener(list);
			}
			btn.repaint();
			btn.revalidate();
		}

	}
	public void set(ArrayList<BtnSetting> list) {
		resetAll();
		if (list == null) {
			setDefaultButtons();
		} else {
			for (BtnSetting b : list) {
				set(b.getBtn(),b.isEnabled(),b.getText(),parseListener(b.getListener()));
				}
		}

	}

	protected ActionListener parseListener(Object object) {
		if (object instanceof ActionListener) {
			return (ActionListener) object;
		}
		else if(object==null||object.equals("")){
			return new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					
				}};
		} else {
			ActionListener ret = null;

			ScriptEngineManager manager = new ScriptEngineManager();
			
			ScriptEngine engine = manager.getEngineByName("JavaScript");
			
			engine.put("gui",gui);
			String script = "var obj = new Object(); obj.actionPerformed = function(e){"
					+ object + "}";
			logger.logMessage("Script is: \n\n"+script+"\n\n==\n");
			try {
				engine.eval(script);
				Object obj = engine.get("obj");
				Invocable inv = (Invocable) engine;
				ret = inv.getInterface(obj, ActionListener.class);
			} catch (Exception e) {
				logger.logError(e);
			}

			return ret;
		}
	}

	private void setDefaultButtons() {
		resetAll();
		ArrayList<BtnSetting> settings = new ArrayList<>();
		settings.add(new BtnSetting(1, true, "New Game", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				gui.newGame();
			}
		}));
		settings.add(new BtnSetting(2, true, "Load", ""));
		settings.add(new BtnSetting(3, true, "Exit", new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				confirmExit(frame);
			}
		}));
		set(settings);
	}

	private void resetAll() {
		for(int x=1;x<11;x++){
		JButton btn=null;
		switch (x) {
		case 1:  btn = btn1;  break;
		case 2:	 btn = btn2;  break;
		case 3:	 btn = btn3;  break;
		case 4:	 btn = btn4;  break;
		case 5:	 btn = btn5;  break;
		case 6:	 btn = btn6;  break;
		case 7:	 btn = btn7;  break;
		case 8:	 btn = btn8;  break;
		case 9:	 btn = btn9;  break;
		case 10: btn = btn10; break;
		default: 
			btn=null;
		}
		btn.setEnabled(false);
		btn.setText("");
		for (ActionListener old : btn.getActionListeners()) {
			btn.removeActionListener(old);
		}
		btn.repaint();
		btn.revalidate();
		}
	}

	public void rebuildGUI() {
		int index = tabbedPane.getSelectedIndex();
		party.rebuildPane();
		party.repaint();
		party.revalidate();
		
		quests.rebuildPane();
		quests.repaint();
		quests.revalidate();
		
		inventory.rebuildPane();
		inventory.repaint();
		inventory.revalidate();
		
		partyTab = new JScrollPane(party);
		tabbedPane
				.setTitleAt(0,
						"<html><body><div style=\"width:50px\">Party</div></body></html>");
		tabbedPane.setComponentAt(0, partyTab);
		
		questTab = new JScrollPane(quests);
		
		tabbedPane
				.setTitleAt(1,
						"<html><body><div style=\"width:50px\">Quests</div></body></html>");
		tabbedPane.setComponentAt(1, questTab);

		inventoryTab = new JScrollPane(inventory);
		tabbedPane
				.setTitleAt(2,
						"<html><body><div style=\"width:50px\">Inventory</div></body></html>");
		tabbedPane.setComponentAt(2, inventoryTab);

		tabbedPane.repaint();
		tabbedPane.revalidate();
		tabbedPane.setSelectedIndex(index);
	}

	public class Controller implements Runnable {
		public Controller(LogHandler log) {
			party = new PartyPane(log);
			new Thread(party).start();
			inventory = new InvPane(log);
			new Thread(inventory).start();
			quests = new QuestPane(log);
			new Thread(quests).start();
			sm = new SceneManager(log);
			new Thread(sm).start();
		}

		public void close() {
			party.close();
			inventory.close();
			quests.close();
			sm.close();
		}

		public void save() {
			// TODO Auto-generated method stub

		}
		public void fetch(String scene){
			Scene s = sm.fetchScene(scene);
			set(s.getButtons());
			String text = s.getText();
			if(debug){
				text+="<h3>"+scene+"</h3>";
			}
			push(text);
		}
		public void newGame() {
			int errors = 0;
			close();
			File gameDB = new File("res/game.db");
			if (gameDB.exists()) {
				File saveDB = new File("data/save.db");
				if (saveDB.exists()) {
					try {
						Files.copy(gameDB.toPath(), saveDB.toPath(),
								StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						errors++;
						logger.logError(e);
					}
				} else {
					try {
						Files.copy(gameDB.toPath(), saveDB.toPath(),
								StandardCopyOption.COPY_ATTRIBUTES);
					} catch (Exception e) {
						errors++;
						logger.logError(e);
					}
				}
			}// gameDB exists
			if (errors == 0) {
				reconnect();
				fetch("new_game");
			} else {
				push("<h1>Whoopsie!</h1><h3>An error occured! Please restart the game and try again. This is likely due to multiple instances of the game running.</h3>");
			}
		}

		private void reconnect() {
			party.reconnect();
			inventory.reconnect();
			quests.reconnect();
			sm.reconnect();
		}

		public void push(String text) {
			if (text == null) {
				text = "(Whoops)";
			}
			
			String setText = "<html><body><div style=\"padding:10px;\">"
					+ text.replaceAll("\n", "<br />") + "</div></body></html>";
			textBox.setText(setText);
		}

		@Override
		public void run() {
		}
		
		@Override
		public String toString(){
			return "GUI Controller";
		}
	}

}

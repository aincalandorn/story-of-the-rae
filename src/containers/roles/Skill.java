package containers.roles;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class Skill {
	private int skillID;
	private int type;
	private int mpCost;
	private int damage;
	private int levelRequired;
	private String description;
	private int str;
	private int dex;
	private int intel;
	private int agi;
	private int end;
	private ArrayList<String> factors;
	
	public Skill(int skillID, int type, int mpCost, int damage, int levelRequired,
			String description, ArrayList<String> factors, int str,
			int dex, int intel, int agi, int end) {
		this.skillID = skillID;
		this.type = type;
		this.mpCost = mpCost;
		this.damage = damage;
		this.levelRequired = levelRequired;
		this.description = description;
		this.factors = factors;
		this.str = str;
		this.dex = dex;
		this.intel = intel;
		this.agi = agi;
		this.end = end;
	}
	public int getSkillID() {
		return skillID;
	}
	public int getMpCost() {
		return mpCost;
	}
	public int getType(){
		return type;
	}
	public int getDamage() {
		//return FormulaCalc.calcDamage(factors, str, dex, intel, agi, end, damage);
		return damage;
	}
	public int getLevelRequired() {
		return levelRequired;
	}
	public String getDescription() {
		return description;
	}
}

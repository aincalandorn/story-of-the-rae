package containers.scenes;

import java.util.ArrayList;

import containers.msgs.BtnSetting;
import containers.msgs.Parsable;

public class Scene {
	private String name;
	private String text;
	private ArrayList<BtnSetting> buttons;
	private int id;
	
	public Scene(String name,int id, String text) {
		this.name = name;
		this.text = text;
		this.setId(id);
		buttons = new ArrayList<>();
	}

	public Scene() {
		buttons = new ArrayList<>();
	}

	public String getName(){
		return name;
	}
	public ArrayList<BtnSetting> getButtons(){
		return buttons;
	}
	public void setText(String string) {
		text = string;	
	}

	public void parse(Parsable p) {
		text.replace(p.getSeek(), p.getReplace());	
	}

	public String getText() {
		return text;
	}
	public void addSetting(BtnSetting s){
		buttons.add(s);
	}
	public void addSettings(ArrayList<BtnSetting> s){
		buttons.addAll(s);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

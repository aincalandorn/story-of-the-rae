package containers.entities;

public class Monster extends Entity {

	public Monster(int entityID, String name, int hp, int mp, int level,
			int classID, int topArmour, int pants, int boots, int hat,
			int weapon, int status, int gender, int dex, int str, int intel,
			int end, int agi, int currHP, int currMP) {
		super(entityID, name, hp, mp, level, classID, topArmour, pants, boots, hat,
				weapon, status, gender, dex, str, intel, end, agi, currHP, currMP);
	}

}

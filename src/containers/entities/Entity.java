package containers.entities;

import java.util.ArrayList;

import containers.roles.Skill;

public class Entity {
	protected int entityID=0;
	protected String name;
	protected int maxHP;
	protected int maxMP;
	protected int hp;
	protected int mp;
	protected int level;
	protected int classID;
	protected int topArmour;
	protected int pants;
	protected int boots;
	protected int hat;
	protected int weapon;
	protected int status;
	protected int gender;
	protected ArrayList<Skill> skills;
	protected int dex;
	protected int str;
	protected int intel;
	protected int end;
	protected int agi;
	
	protected boolean inParty=false;
	
	public Entity(int entityID, String name, int hp, int mp, int level,
			int classID, int topArmour, int pants, int boots, int hat,
			int weapon, int status, int gender, int dex, int str, int intel,
			int end, int agi, ArrayList<Skill> skills, int currHP, int currMP) {
		this.entityID = entityID;
		this.name = name;
		this.maxHP = hp;
		this.maxMP = mp;
		this.level = level;
		this.classID = classID;
		this.topArmour = topArmour;
		this.pants = pants;
		this.boots = boots;
		this.hat = hat;
		this.weapon = weapon;
		this.status = status;
		this.gender = gender;
		this.dex = dex;
		this.str = str;
		this.intel = intel;
		this.end = end;
		this.agi = agi;
		this.skills = skills;
		this.hp=currHP;
		this.mp=currMP;
	}
	
	public Entity(int entityID, String name, int hp, int mp, int level,
			int classID, int topArmour, int pants, int boots, int hat,
			int weapon, int status, int gender, int dex, int str, int intel,
			int end, int agi,int currHP, int currMP) {
		this.entityID = entityID;
		this.name = name;
		this.maxHP = hp;
		this.maxMP = mp;
		this.level = level;
		this.classID = classID;
		this.topArmour = topArmour;
		this.pants = pants;
		this.boots = boots;
		this.hat = hat;
		this.weapon = weapon;
		this.status = status;
		this.gender = gender;
		this.dex = dex;
		this.str = str;
		this.end = end;
		this.agi = agi;
		this.intel = intel;
		skills=new ArrayList<Skill>();
		this.hp=currHP;
		this.mp=currMP;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public int getAgi() {
		return agi;
	}

	public void setAgi(int agi) {
		this.agi = agi;
	}

	public int getDex() {
		return dex;
	}

	public void setDex(int dex) {
		this.dex = dex;
	}

	public int getStr() {
		return str;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public int getIntel() {
		return intel;
	}

	public void setIntel(int intel) {
		this.intel = intel;
	}

	public ArrayList<Skill> getSkills(){
		return skills;
	}
	public void addSkill(Skill s){
		skills.add(s);
	}
	public void addSkills(ArrayList<Skill> s){
		skills.addAll(s);
	}
	public void resetSkills(){
		skills.clear();
	}
	public int getEntityID() {
		return entityID;
	}
	public void setEntityID(int entityID) {
		this.entityID = entityID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMaxHp() {
		return maxHP;
	}
	public void setMaxHp(int hp) {
		this.maxHP = hp;
	}
	public int getMaxMp() {
		return maxMP;
	}
	public void setMaxMp(int mp) {
		this.maxMP = mp;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getClassID() {
		return classID;
	}
	public void setClassID(int classID) {
		this.classID = classID;
	}
	public int getTopArmour() {
		return topArmour;
	}
	public void setTopArmour(int topArmour) {
		this.topArmour = topArmour;
	}
	public int getPants() {
		return pants;
	}
	public void setPants(int pants) {
		this.pants = pants;
	}
	public int getBoots() {
		return boots;
	}
	public void setBoots(int boots) {
		this.boots = boots;
	}
	public int getHat() {
		return hat;
	}
	public void setHat(int hat) {
		this.hat = hat;
	}
	public int getWeapon() {
		return weapon;
	}
	public void setWeapon(int weapon) {
		this.weapon = weapon;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getMp() {
		return mp;
	}

	public void setMp(int mp) {
		this.mp = mp;
	}

	public boolean inParty() {
		return inParty;
	}

	public void setParty(boolean inParty) {
		this.inParty = inParty;
	}
	
}

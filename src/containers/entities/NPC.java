package containers.entities;

import java.util.ArrayList;

import containers.roles.Skill;

public class NPC extends Entity{
	private int exp;
	private int toLevel;
	public NPC(int entityID, String name, int hp, int mp,int exp,
			int toLevel, int level, int classID, int topArmour, int pants,
			int boots, int hat, int weapon, int status, int gender, int dex,
			int str, int intel, int end, int agi, ArrayList<Skill> skills, int currHP, int currMP) {
		super(entityID, name, hp, mp, level, classID, topArmour, pants, boots,
				hat, weapon, status, gender, dex, str, intel, end, agi, skills, currHP, currMP);
		this.exp = exp;
		this.toLevel = toLevel;
	}
	
	

	@Override
	public String toString() {
		return "NPC [exp=" + exp + ", toLevel=" + toLevel
				+ ", entityID=" + entityID + ", name=" + name + ", maxHP="
				+ maxHP + ", maxMP=" + maxMP + ", hp=" + hp + ", mp=" + mp
				+ ", level=" + level + ", classID=" + classID + ", topArmour="
				+ topArmour + ", pants=" + pants + ", boots=" + boots
				+ ", hat=" + hat + ", weapon=" + weapon + ", status=" + status
				+ ", gender=" + gender + ", skills=" + skills + ", dex=" + dex
				+ ", str=" + str + ", intel=" + intel + ", end=" + end
				+ ", agi=" + agi + ", inParty=" + inParty + "]";
	}



	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public int getToLevel() {
		return toLevel;
	}
	public void setToLevel(int toLevel) {
		this.toLevel = toLevel;
	}
}

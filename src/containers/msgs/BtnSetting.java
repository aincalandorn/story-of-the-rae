package containers.msgs;

import java.awt.event.ActionListener;

public class BtnSetting {
	int btn = 0;
	boolean enabled = false;
	String text = "";
	String list = "";
	ActionListener listener = null;

	public BtnSetting(int btn, boolean enabled, String text, String list) {
		this.btn = btn;
		this.enabled = enabled;
		this.text = text;
		this.list = list;
	}
	public BtnSetting(int btn, boolean enabled, String text, ActionListener listener) {
		this.btn = btn;
		this.enabled = enabled;
		this.text = text;
		this.listener = listener;
	}
	public int getBtn() {
		return btn;
	}

	public void setBtn(int btn) {
		this.btn = btn;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Object getListener() {
		if(list!=null&&!list.equals("")){
			return list;
		}
		else{
			return listener;
		}
	}
	public void setListener(ActionListener list) {
		this.listener = list;
	}
	public void setListener(String list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "BtnSetting [btn=" + btn + ", enabled=" + enabled + ", text="
				+ text + ", list=" + list + ", listener="+listener+"]";
	}
}

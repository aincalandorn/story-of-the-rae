package containers.msgs;

public class Parsable {
	private String seek;
	private String replace;
	public Parsable(String toReplace, String replace) {
		seek = toReplace;
		this.replace = replace;
	}
	public String getSeek(){
		return seek;
	}
	public String getReplace(){
		return replace;
	}

}

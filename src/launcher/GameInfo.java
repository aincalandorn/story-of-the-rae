package launcher;

public class GameInfo {
	private final String name = "Story of the Rae";
	private final String versionName = "GUI Rebuild, part 2";
	private final double versionNum = 0.06;
	private final String buildDate = "January 9, 2015";
	
	private final String credits = "This game was inspired by Alder's Fall of Eden (<strong>http://www.fenoxo.com/category/fall-of-eden</strong>) and"
			+ " Lithier's My Very Own Lith (<strong>http://mvolith.blogspot.ca</strong>) games.\n\nWhile both are adult in nature, this story will"
			+ " not contain sexual content. The game is very much a work in progress as the world is built.\n\n"
			+ "To read more about the world the game is set in, visit the wiki (<strong>http://lang.ainstower.com/</strong>)\n\n"
			+ "I would also like to give thanks to: \n<ul>"
			+ "<li>SubPac for proofreading the content (and introducing me to FoE)</li>"
			+ "<li>Javara for helping me flesh out ideas for the world/language and artwork</li>"
			+ "<li>Chi for helping me flesh out ideas for the world/language and artwork</li>"
			+ "<li>Majo for the RP that started the entire idea</li>"
			+ "<li>Megan for the artwork</li>"
			+ "<li>Nyra for helping me flesh out ideas for the world/language and artwork</li>"
			+ "</ul>";
	
	public String getTitle() {
		return name+" "+versionNum+": "+versionName+" (Built on: "+buildDate+")";
	}

	public double getVersionNum() {
		return versionNum;
	}
	public String getInfo(){
		String ret = "<h2>The game has debugging enabled!</h2>";
		ret+="<hr />\nBuild info: \n<blockquote>";
		ret+= "Version: "+versionNum+"\n";
		ret+= "Version name: "+versionName+"\n";
		ret+= "</blockquote>\n<hr />";
		return ret;
	}
	
	public String getCredits(){
		return "<div>"+credits+"</div>";
	}
}
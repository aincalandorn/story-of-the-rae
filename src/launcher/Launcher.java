package launcher;

import java.io.File;
import java.nio.file.*;

import gui.GUI;
import handlers.LogHandler;

public class Launcher {

	public static void main(String[] args) {
		
		boolean debug=false;
		for(String s : args){
			if(s.equalsIgnoreCase("debug")){
				debug=true;
				
			}
		}
		LogHandler logger = new LogHandler(debug);
		
		int errors = 0;
		File gameDB = new File("./res/game.db");
		if(gameDB.exists()){
			logger.logMessage("GameDB exists!");
			File saveDB = new File("./data/save.db");
			if(!saveDB.exists()){
				logger.logMessage("Creating SaveDB");
				try {
					Files.copy(gameDB.toPath(), saveDB.toPath(),StandardCopyOption.COPY_ATTRIBUTES);
				} catch (Exception e) {
					errors++;
					logger.logError(e);
				}
			}
			if(errors>0){
				System.out.println("Errors: "+errors);
				System.exit(errors);
			}
			else{
				logger.logMessage("DB Created!");
				logger.logMessage("Loading GUI!");
				new GUI(debug,new GameInfo(),logger);
			}
		}
		else{
			logger.logError("GameDB missing!");
		}
	}

}
